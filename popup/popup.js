var popup = {
  
  tags: {},
  views: {},
  active_view: null,
  master_key_set: null,
  
  init: function() {
    var views = document.getElementsByClassName("view");
    for (var i=0; i<views.length; i++) {
      var m = views[i].className.match(/view-([a-z0-9\-]+)/);
      this.views[m[1]] = views[i];
      if (views[i].className.match("active"))
        this.active_view = m[1];
    }
    
    // Back buttons
    var backs = document.getElementsByClassName("btn-back");
    for (var i=0; i<backs.length; i++) {
      backs[i].addEventListener("click", function() {
        popup.view("default");
      }, false);
    }
    
    // View: default
    this.tags.btn_key_clear = document.getElementById("btn-key-clear");
    this.tags.btn_key_clear.addEventListener("click", function() {
      popup.clearMasterKey();
    }, false);
    var btn = document.getElementById("btn-key");
    btn.addEventListener("click", function() {
      popup.view("master-key");
    }, false);
    
    // View: master-key
    this.tags.master_form = document.getElementById("master-key-form");
    this.tags.master_form.addEventListener("submit", function(e) {
      e.preventDefault();
      popup.saveMasterKey(popup.tags.master_form);
    }, false);
    this.tags.master_form.master_key.addEventListener("keyup", function() {
      popup.showKeyStrength("key", popup.tags.master_form.master_key);
    }, false);
    
    // View: custom service
    this.tags.custom_form = document.getElementById("custom-service-form");
    var btn = document.getElementById("btn-custom");
    var copy = document.getElementById("copy-custom");
    btn.addEventListener("click", function() {
      popup.view("custom-service");
    }, false);
    copy.addEventListener("click", function() {
      popup.copyCustom(copy.previousElementSibling);
    }, false);
    this.tags.custom_form.addEventListener("submit", function(e) {
      e.preventDefault();
      popup.createCustom(popup.tags.custom_form);
    }, false);
    this.tags.custom_form.key.addEventListener("keyup", function() {
      popup.showKeyStrength("custom", popup.tags.custom_form.key);
    }, false);
    
    // Check master key
    MasterKey.getMasterKey().then(function(r) {
      popup.master_key_set = true;
      popup.view("default");
    }, function() {
      popup.view("default");
    });
  },
  
  view: function(name, obj) {
    if (!this.views[name])
      throw "View not found: "+name;
    if (this.active_view)
      this.views[this.active_view].classList.remove("active");
    this.views[name].classList.add("active");
    this.active_view = name;
    var focus = this.views[name].getElementsByClassName("focus");
    if (focus.length)
      focus[0].focus();
    
    if (name == "default") {
      if (this.master_key_set) {
        this.views[name].classList.add("key-set");
        this.tags.btn_key_clear.classList.remove("inactive");
      }
      else {
        this.views[name].classList.remove("key-set");
        this.tags.btn_key_clear.classList.add("inactive");
      }
    }
    
    if (name == "custom-service") {
      this.clearMessage("custom");
      var form = this.tags.custom_form;
      form.key.value = form.service.value = "";
      form.account.value = form.password.value = "";
      form.key.classList.remove("error");
      form.service.classList.remove("error");
      form.account.classList.remove("error");
      form.password.parentNode.classList.add("inactive");
      if (this.master_key_set) {
        form.key.parentNode.classList.add("inactive");
        form.service.focus();
      }
      else {
        form.key.parentNode.classList.remove("inactive");
        form.key.focus();
      }
    }
  },
  
  clearMasterKey: function() {
    MasterKey.clearMasterKey().then(function() {
      popup.master_key_set = false;
      popup.view("default");
    }, function() {
      popup.setMessage("status", "Failed to unset master key", "error");
    });
  },
  
  saveMasterKey: function(form) {
    var master_key = form.master_key.value;
    var secs = MasterKey.secondsToCrack(master_key);
    if (secs < 31536000) {
      this.setMessage("key", "Password too weak.", "error");
      return;
    }
    form.master_key.value = "";
    this.setMessage("key", "Hashing password...", "loading");
    MasterKey.setMasterKey(master_key).then(function() {
      popup.setMessage("key", "");
      popup.master_key_set = true;
      popup.view("default");
    }, function() {
      popup.setMessage("key", "Failed!", "error");
    });
  },
  
  showKeyStrength: function(msg, el) {
    var key = el.value;
    if (key.length) {
      var secs = MasterKey.secondsToCrack(key);
      var msg_type = "info";
      if (secs < 31536000)
        msg_type = "error";
      else if (secs < 10*31536000)
        msg_type = "warning";
      var str = MasterKey.secondsToString(secs, 2);
      popup.setMessage(msg, "Time to crack key:<br>"+str, msg_type);
    }
    else {
      popup.clearMessage(msg);
    }
  },
  
  createCustom: function(form) {
    this.clearMessage("custom");
    for (var i=0; i<form.elements.length; i++)
      form.elements[i].classList.remove("error");
    form.password.parentNode.classList.add("inactive");
    
    if (!this.master_key_set && !form.key.value.length) {
      form.key.classList.add("error");
      form.key.focus();
      return;
    }
    if (!form.service.value.length) {
      form.service.classList.add("error");
      form.service.focus();
      return;
    }
    if (!form.account.value.length) {
      form.account.classList.add("error");
      form.account.focus();
      return;
    }
    
    this.setMessage("custom", "Generating password", "loading");
    var onSuccess = function(key) {
      var password = MasterKey.generatePassword(key, form.service.value, form.account.value);
      form.password.value = password;
      form.password.parentNode.classList.add("far-away");
      form.password.parentNode.classList.remove("inactive");
      form.password.focus();
      form.password.select();
      form.password.parentNode.classList.remove("far-away");
      popup.clearMessage("custom");
    };
    var onError = function() {
      popup.setMessage("custom", "Something went wrong...");
    };
    if (form.key.value.length)
      onSuccess(MasterKey.hashKey(form.key.value));
    else
      MasterKey.getMasterKey().then(onSuccess, onError);
  },
  
  copyCustom: function(input) {
    input.focus();
    input.select();
    if (document.execCommand("copy")) {
      input.blur();
      popup.setMessage("custom", "Password copied to clipboard", "success");
    }
    else {
      popup.setMessage("custom", "Failed to copy", "error");
    }
  },
  
  setMessage: function(id, msg, type) {
    var el = document.getElementById("msg-"+id);
    if (!el)
      throw "Message element not found: "+id;
    el.className = el.className.replace(/ msg-[a-z]+/, "");
    el.classList.add("msg-"+type);
    el.classList.add("active");
    el.innerHTML = msg;
  },
  
  clearMessage: function(id) {
    var el = document.getElementById("msg-"+id);
    el.classList.remove("active");
  },
  
};
popup.init();