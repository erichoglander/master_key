var MasterKey = {
  
  /**
   * These salts are the same for everyone. 
   * They are used to hash a key.
   * @var array
   */
  MASTER_SALTS: [
    "Y6whM4+#s^-psu+Bq$*G$58teUd+n!=Y",
    "x=*_zGdv3CRu@$$D4Ksu#3#3?mph=AuM"
  ],
  
  /**
   * These are the characters used in passwords
   * Changing this will also change the generated passwords
   * We ignore some characters because some (bad) sites have problems characters
   * such as whitespace, slashes, or quotes
   * @var string
   */
  PASSWORD_CHARACTERS: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789§!#%&()[]{}=¤@£$+?-_.:,;<>*^~",
  
  
  /**
   * Max length of a password
   * Changing this will also change the generated passwords
   * Default is 32, which is a common max length on old shitty websites
   * Number of password combinations with default values will still be 91^32
   * Maybe this should be an option...
   * @var int
   */
  PASSWORD_MAX_LENGTH: 32,
  
  
  /**
   * Estimated number of seconds to break a password
   * by bruteforcing an md5 hash with hashcat with a modern PC
   * @param  string
   * @return int
   */
  secondsToCrack: function(password) {
    // Common passwords taken from http://www.passwordrandom.com/most-popular-passwords
    var common = [
      "password", "password1", "password123", "password1234",
      "1234", "12345", "123456", "1234567", "12345678", "123456789", "1234567890",
      "dragon", "pussy", "baseball", "football", "letmein", "monkey",
      "696969", "abc", "abc123", "mustang", "michael", "shadow",
      "master", "jennifer", "111111", "2000", "jordan", "superman",
      "harley", "fuckme", "hunter", "fuckyou", "trustno1", "ranger",
      "buster", "thomas", "tigger", "robert", "soccer", "fuck", "batman",
      "test", "pass", "killer", "hockey", "george", "charlie", "andrew",
      "michelle", "love", "sunshine", "jessica", "asshole", "6969",
    ];
    if (common.indexOf(password) != -1)
      return 0;
    var len = password.length;
    var has_simple_special = password.match(/[\-\_\+\!\?\ ]/);
    var has_better_special = password.match(/[^A-Za-z0-9\-\_\+\!\?\ ]/);
    var has_number = password.match(/[0-9]/);
    var has_upper = password.match(/[A-Z]/);
    var has_lower = password.match(/[a-z]/);
    var num = 0;
    if (has_upper)
      num+= 26;
    if (has_lower)
      num+= 26;
    if (has_number)
      num+= 10;
    if (has_simple_special)
      num+= 6;
    if (has_better_special)
      num+= 20; // estimated
    var combinations = Math.pow(num, len);
    var hashes_per_second = 5*40000000000; // 40 billion per gtx 1080 and 5 cards
    return Math.round(combinations/hashes_per_second);
  },
  
  /**
   * Generates password from master key
   * @see    generatePassword
   * @param  string service
   * @param  string account
   * @return promise
   */
  generatePasswordFromMasterKey: function(service, account) {
    return new Promise(function(resolve, reject) {
      MasterKey.getMasterKey().then(function(master_key) {
        resolve(MasterKey.generatePassword(master_key, service, account));
      }, function() {
        reject();
      });
    });
  },
  
  /** 
   * Generates a password form key, service, and account name7
   * 1. Hash account name with sha-512 with the service as HMAC-key
   * 2. Hash the first hash with sha3-512 with the key as HMAC-key
   * 3. Convert that hash to a password
   * @see    hashToPassword
   * @param  string key
   * @param  string service
   * @param  string account
   * @return string
   */
  generatePassword: function(key, service, account) {
    service = service.toLocaleLowerCase();
    account = account.toLocaleLowerCase();
    var hash_account = new jsSHA("SHA-512", "TEXT");
    var hash_final = new jsSHA("SHA3-512", "TEXT");
    hash_account.setHMACKey(service, "TEXT");
    hash_account.update(account);
    hash_final.setHMACKey(key, "TEXT");
    hash_final.update(hash_account.getHMAC("HEX"));
    return this.hashToPassword(hash_final.getHMAC("HEX"));
  },
  
  /**
   * Converts a hash into a shorter password with more characters
   * @param  string hash
   * @return string
   */
  hashToPassword: function(hash) {
    var dec = parseInt(hash, 16);
    if (isNaN(dec))
      throw "Invalid hash";
    var len = this.PASSWORD_CHARACTERS.length;
    var password = "";
    var index;
    while (dec) {
      index = dec%len;
      password+= this.PASSWORD_CHARACTERS[index];
      dec = (dec-index)/len;
    }
    return password.substr(0, this.PASSWORD_MAX_LENGTH);
  },
   
  /**
   * Get master key
   * @return promise
   */
  getMasterKey: function() {
    return new Promise(function(resolve, reject) {
      browser.storage.local.get("master_key").then(function(obj) {
        if (Array.isArray(obj))
          obj = obj[0];
        if (obj.master_key)
          resolve(obj.master_key);
        else
          reject();
      }, function() {
        reject();
      });
    });
  },
  
  /**
   * Set master key
   * @param  string  Key in clear text that we want to hash and store
   * @return promise
   */
  setMasterKey: function(key) {
    var master = this.hashKey(key);
    return this.setHashedMasterKey(master);
  },
  
  /**
   * Set the already hashed master key
   * @param  string
   * @return promise
   */
  setHashedMasterKey: function(master) {
    return new Promise(function(resolve, reject) {
      browser.storage.local.set({
        master_key: master,
      }).then(function(item) {
        resolve();
      }, function() {
        reject();
      });
    });
  },
  
  /**
   * Removed master key from storage
   * @return promise
   */
  clearMasterKey: function() {
    return browser.storage.local.remove("master_key");
  },
  
  /**
   * Hash key
   * We run it through sha-512 and sh3a-512 since we don't know
   * which one is more secure right now but both should be enough
   * @param  string Key in clear text that we want to hash and store
   * @return string
   */
  hashKey: function(key) {
    var hash = new jsSHA("SHA-512", "TEXT");
    var hash2 = new jsSHA("SHA3-512", "TEXT");
    hash.setHMACKey(this.MASTER_SALTS[0], "TEXT");
    hash.update(key);
    hash2.setHMACKey(this.MASTER_SALTS[1], "TEXT")
    hash2.update(hash.getHMAC("HEX"));
    return hash2.getHMAC("HEX");
  },
  
  /**
   * Converts number of seconds to a readable string of time
   * Ex: 3687 seconds -> 1 hour, 1 minute, 27 seconds
   * @param  int
   * @param  int    Number of units
   * @return string
   */
  secondsToString: function(secs, num_units) {
    if (secs > 31536000 * 14000000000)
      return "Longer than the age of the universe";
    var units = [
      {
        seconds: 31536000,
        singular: "year",
        plural: "years",
        short: "y",
      },
      {
        seconds: 604800,
        singular: "week",
        plural: "weeks",
        short: "w",
      },
      {
        seconds: 86400,
        singular: "day",
        plural: "days",
        short: "d",
      },
      {
        seconds: 3600,
        singular: "hour",
        plural: "hours",
        short: "h",
      },
      {
        seconds: 60,
        singular: "minute",
        plural: "minutes",
        short: "m",
      },
      {
        seconds: 1,
        singular: "second",
        plural: "seconds",
        short: "s",
      },
    ];
    var high_names = [
      {
        number: Math.pow(10, 9),
        name: "billion",
      },
      {
        number: Math.pow(10, 6),
        name: "million",
      },
    ];
    var arr = [];
    var n, x, str;
    for (var i=0; i<units.length; i++) {
      n = Math.floor(secs / units[i].seconds);
      if (n) {
        // Check for high value names
        if (i == 0) {
          for (var j=0; j<high_names.length; j++) {
            if (high_names[j].number < n)
              return Math.floor(n/high_names[j].number)+" "+high_names[j].name+" "+units[i].plural;
          }
        }
        // Regular units, formatted for singular / plural
        str = n;
        if (n == 1)
          str+= " "+units[i].singular;
        else
          str+= " "+units[i].plural;
        arr.push(str);
        // Check number of units
        if (num_units && num_units == arr.length)
          break;
        secs-= n * units[i].seconds;
      }
    }
    if (!arr.length)
      return "Less than a second";
    return arr.join(", ");
  },
  
  /**
   * Strip down an url to the domain
   * @param  string url
   * @return
   */
  domainFromUrl: function(url) {
    url = url.replace(/^https?\:\/\/(www\.)?/, "");
    url = url.replace(/\/.*$/, "");
    return url;
  },
  
};