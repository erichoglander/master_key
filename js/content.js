var MasterKeyContent = {

  HOTKEY: 17, // CTRL
  HOTKEY_INTERVAL: 300,

  hotkey_timer: 0,
  service: null,

  init: function() {
    this.service = window.location.host.replace(/^www\./, "");
    window.addEventListener("keyup", function(e) {
      MasterKeyContent.onKeyUp(e);
    }, false);
  },

  onKeyUp: function(e) {
    if (e.keyCode == this.HOTKEY) {
      var t = Date.now();
      if (t - this.hotkey_timer < this.HOTKEY_INTERVAL)
        this.fill();
      else
        this.hotkey_timer = t;
    }
  },
  
  fill: function(el) {
    if (!el)
      el = document.activeElement;
    var input_password, input_account, account;
    if (el && el.tagName == "INPUT") {
      if (el.type == "password")
        input_password = el;
      else
        input_account = el;
    }
    if (!input_password) {
      input_password = this.findPassword(el, input_account);
      if (!input_password) {
        alert("Could not find password field");
        return;
      }
    }
    if (!input_account) {
      if (input_password.tagName)
        input_account = this.findAccount(input_password);
      else
        input_account = this.findAccount(input_password[0]);
    }
    if (!input_account) {
      alert("Could not find account name field");
      return;
    }
    account = input_account.value.trim();
    if (!account) {
      account = prompt("Enter username").trim();
      if (!account)
        return;
    }
    MasterKey.generatePasswordFromMasterKey(this.service, account).then(function(password) {
      if (input_password.tagName) {
        input_password.value = password;
      }
      else {
        for (var i=0; i<input_password.length; i++)
          input_password[i].value = password;
      }
      input_account.value = account;
    }, function() {
      console.log("Master key not set");
    });
  },
  
  findPassword: function(el, input_account) {
    for (el=el; el && (el.getElementsByTagName || el.parentNode); el = el.parentNode) {
      if (!el.getElementsByTagName)
        continue;
      var inputs = el.getElementsByTagName("input");
      var index = null;
      var found = (input_account ? false : true);
      for (var i=0; i<inputs.length; i++) {
        if (!found) {
          if (inputs[i] == input_account)
            found = true;
          else
            continue;
        }
        if (inputs[i].type == "password") {
          if (index === null)
            index = i;
          else
            return [inputs[index], inputs[i]];
        }
      }
      if (index !== null)
        return inputs[index];
    }
    return null;
  },
  
  findAccount: function(el) {
    var pass = (el.tagName == "INPUT" && el.type == "password" ? el : null);
    var types = ["text", "email", "tel"];
    for (el=el; el && (el.getElementsByTagName || el.parentNode); el = el.parentNode) {
      if (!el.getElementsByTagName)
        continue;
      var inputs = el.getElementsByTagName("input");
      if (inputs.length) {
        if (pass) {
          for (var i=0; i<inputs.length; i++) {
            var index = null;
            if (i > 0 && inputs[i].type == "password" && pass) {
              index = i;
              break;
            }
          }
          if (index !== null) {
            for (var i=index-1; i>=0; i--) {
              if (types.indexOf(inputs[i].type.toLowerCase()) != -1)
                return inputs[i];
            }
          }
        }
        else {
          for (var i=0; i<inputs.length; i++) {
            if (types.indexOf(inputs[i].typee.toLowerCase()) != -1)
              return inputs[i];
          }
        }
      }
    }
    return null;
  },
  
};

MasterKeyContent.init();